Shader "Unlit/Background Shader"
{
    Properties
    {
        [NoScaleOffset] _Stars ("Stars Texture", 2D) = "white" {}
        [NoScaleOffset] _StarsNoise ("Stars Noise", 2D) = "white" {}
        _StarsDensity ("Stars Density", Range(1, 10)) = 1
        [NoScaleOffset] _NebulaNoise ("Nebula Noise", 2D) = "white" {}
        _Color1 ("Nebula Color 1", Color) = (1,1,1,1)
        _Density1 ("Density 1" , Range(1, 10)) = 1
        [ShowAsVector2] _Direction1 ("Velocity 1", Vector) = (0,0,0,0)
        _Color2 ("Nebula Color 2", Color) = (1,1,1,1)
        _Density2 ("Density 2" , Range(1, 10)) = 1
        [ShowAsVector2] _Direction2 ("Velocity 2", Vector) = (0,0,0,0)
    }
    SubShader
    {
        Tags { "Queue" = "Background" "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            /*CGINCLUDE
                float4 noise (float2 uv, sampler2D tex float2 size, float4 color, float intensity)
                {
                    float2 UV = uv * size;
                    float4 nebulaTexture = tex2D(tex, UV);
                    return pow(nebulaTexture * color, intensity);
                }
            ENDCG*/

            Name "Stars"
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct VertexInput
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            sampler2D _Stars;
            sampler2D _StarsNoise;
            float _StarsDensity;

            sampler2D _NebulaNoise;
            float _Density1;
            float2 _Direction1;
            float4 _Color1;
            float _Density2;
            float2 _Direction2;
            float4 _Color2;

            v2f vert (VertexInput v) 
            {
                v2f o;
                o.uv = v.uv;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag (v2f o) : SV_Target
            {
                //Time
                float time = _Time[1];

                //Stars Texture
                float2 size = o.uv * float2(0.85,1.5);
                float4 starsTexture = tex2D(_Stars, size);
                float2 noiseDir = (size / 10) + (time / 500); //Movement
                float4 starsNoiseTexture = tex2D(_StarsNoise, noiseDir);
                //result
                float4 stars = pow(starsTexture * starsNoiseTexture, _StarsDensity);

                //Nebula Texture Number 1
                float2 UV1 = o.uv * float2(0.5, 1);
                float2 nebulaDir1 = UV1 + (_Direction1 * (time / 100)); //Movement
                float4 nebulaTexture1 = tex2D(_NebulaNoise, nebulaDir1);
                //result
                float4 noise1 = nebulaTexture1 * _Color1; 

                //Nebula Texture Number 2
                float2 UV2 = o.uv * float2(-0.5, 1);
                float2 nebulaDir2 = UV2 + (_Direction2 * (time / 100));
                float4 nebulaTexture2 = tex2D(_NebulaNoise, nebulaDir2);
                //result
                float4 noise2 = nebulaTexture2 * _Color2;

                //Combine All
                float4 nebulas = pow(noise1, _Density1) + pow(noise2, _Density2);
                return nebulas + stars;
            }

            ENDCG
        }
    }
}