using System;
using UnityEngine;
using UnityEngine.InputSystem;

[Serializable] public struct LevelProperties
{
    public Material mats;
    [Range(0, 10)] public float separation, speed;
    [Range(0, 20)] public float swipeDistance;
    public InputAction startTap, stopTap, swipe;
}
[Serializable] public struct LevelStation
{
    public Sprite station;
    [Range(0,1000)] public float levelLenth;
}
public class LevelSelector : MonoBehaviour
{
    [SerializeField] private int currentLevel;
    [SerializeField] private LevelProperties view;
    [SerializeField] private LevelStation[] stations;

    [HideInInspector] public bool canSwipe;
    private int progress, length;
    private bool swipe;
    private Transform _transform;
    private User_Interface ui;

    private void Awake()
    {
        #region Input Swipe System
        view.startTap.performed += ctx => swipe = true;
        view.stopTap.performed += ctx => swipe = false;
        view.swipe.performed += ctx => Swipe(ctx.ReadValue<float>(), view.swipeDistance);
        #endregion
        progress = currentLevel = Player.progress;
        length = stations.Length;
        _transform = transform;
        ui = GameObject.FindGameObjectWithTag("Canvas").GetComponent<User_Interface>();

        #region Instantiate Elements
        for (int i = 0; i < length; i++)
        {
            GameObject station = new GameObject("station_" + i);
            SpriteRenderer render = station.AddComponent<SpriteRenderer>();
            render.sprite = stations[i].station;
            if (i > progress) render.material = view.mats;

            Transform module = station.transform;
            module.parent = _transform;
            module.localScale = Vector3.one;
            module.localPosition = Vector3.right * view.separation * i;
        }
        #endregion
        _transform.position += Vector3.left * currentLevel * view.separation;
    }
    private void OnEnable()
    {
        view.swipe.Enable(); view.startTap.Enable(); view.stopTap.Enable();
    }
    private void OnDisable()
    {
        view.swipe.Disable(); view.startTap.Disable(); view.stopTap.Disable();
    }

    private void Update()
    {
        Vector3 pos = _transform.position;
        pos.x = Mathf.Lerp(pos.x, currentLevel * -view.separation, Time.deltaTime * view.speed);
        _transform.position = pos;
    }
    private void Swipe(float delta, float detection)
    {
        if (!canSwipe) return;

        if (Mathf.Abs(delta) > detection && swipe)
        {
            int dir = (delta > 0) ? -1 : 1;
            currentLevel = Mathf.Clamp(currentLevel += dir, 0, length - 1);
            swipe = false;
        }
    }
    public void SetLevelValues()
    {
        if (currentLevel <= progress)
        {
            Progress._levelSize = stations[currentLevel].levelLenth;
            ui.ChangeScene("Gameplay");
        }
    }
}