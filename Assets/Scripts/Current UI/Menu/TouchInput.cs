﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

[Serializable] public struct TextInputValues
{
    public string message;
    public TextMeshProUGUI text;
    public LeanTweenType animation;
    public Color color;
    public InputAction tap;
}
public class TouchInput : MonoBehaviour
{
    [SerializeField] TextInputValues input;

    private void Awake()
    {
        input.tap.performed += ctx => Destroy(this.gameObject);

        input.text.text = input.message;
        Color color = input.color;

        LTDescr text = LeanTween.value(0, 1, 1);
        text.setOnUpdate((float val) => input.text.color = new Color(color.r, color.g, color.b, val));
        text.setEase(input.animation);
        text.setLoopPingPong();
    }
    private void OnEnable()
    {
        input.tap.Enable();
    }
    private void OnDisable()
    {
        input.tap.Disable();
    }
}