﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

[Serializable] public struct MarsAnimation
{
    [Header("UI Canvas")]
    public Transform canvas;
    public GameObject missinUIPrefab;
    public GameObject touchInput, touchInputPrefab;

    [Header("Transition Animation")]
    public LeanTweenType animation;
    [Range(0, 25)] public float newSize;
    [Range(0, 5)] public float time;

    [Header("3D Elements")]
    public Transform mars;
    public Transform station;
    [Range(0, 20)] public float rotSpeed;
}
public class TransitionAnimation : MonoBehaviour
{
    private User_Interface ui;
    [SerializeField] MarsAnimation anim;
    static public bool _changeScene;

    private void Start()
    {
        ui = anim.canvas.GetComponent<User_Interface>();
        if (!_changeScene) StartCoroutine(Transition()); else MainMenu();
    }
    private void Update()
    {
        float rot = anim.mars.localEulerAngles.y;
        rot += Time.deltaTime;
        anim.mars.localEulerAngles = new float3(0, rot, 0);
    }
    private void MainMenu()
    {
        ui.SetCurrentUI(1);
        Destroy(anim.touchInput);

        float3 scale = new float3(1, 1, 1);
        anim.mars.localScale = scale * anim.newSize;
        anim.station.GetComponent<LevelSelector>().canSwipe = true;
        
        for (int i = 0; i < anim.station.childCount; i++)
            anim.station.GetChild(i).transform.localScale = scale;
    }
    private IEnumerator Transition()
    {
        ui.SetCurrentUI(0);
        anim.station.GetComponent<LevelSelector>().canSwipe = false;

        List<Transform> stations = new List<Transform>();
        for (int i = 0; i < anim.station.childCount; i++) 
            stations.Add(anim.station.GetChild(i).transform);

        for (int i = 0; i < stations.Count; i++) stations[i].localScale = Vector3.zero;

        yield return new WaitUntil(() => anim.touchInput == null);

        GameObject mision = Instantiate(anim.missinUIPrefab, anim.canvas);
        ui.InstanceUI(anim.touchInputPrefab);

        yield return new WaitUntil(() => mision == null);
        
        float time = 0;
        _changeScene = true;
        
        float3 mars = anim.mars.localScale;
        float3 station = new float3(1, 1, 1);

        LeanTween.value(0, 1, anim.time).setOnUpdate((float val) => time = val).setEase(anim.animation);

        do
        {
            anim.mars.localScale = mars + ((anim.newSize - mars) * time);
            for (int i = 0; i < stations.Count; i++) stations[i].localScale = station * time;

            yield return new WaitForEndOfFrame();
        } 
        while (time < 1);
        
        ui.SetCurrentUI(1);
        anim.station.GetComponent<LevelSelector>().canSwipe = true;
    }
}