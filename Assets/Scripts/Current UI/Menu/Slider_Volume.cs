﻿using UnityEngine;
using UnityEngine.UI;

public class Slider_Volume : MonoBehaviour
{
    public Slider _mainVolume, _effectVolume;
    public Toggle _mute;

    private void Awake()
    {
        _mainVolume.value = Audio_Manager._mainVolume;
        _effectVolume.value = Audio_Manager._effectVolume;
        _mute.isOn = Audio_Manager._mute;
    }
    public void SliderVolume(int slide)
    {
        switch (slide)
        {
            case 0: Audio_Manager._mainVolume = _mainVolume.value; break;
            case 1: Audio_Manager._effectVolume = _effectVolume.value; break;
            default: Debug.Log(message: $"value is grater than sliders {slide}"); break;
        }
    }
    public void Mute()
    {
        Audio_Manager._mute = _mute.isOn;
    }
}