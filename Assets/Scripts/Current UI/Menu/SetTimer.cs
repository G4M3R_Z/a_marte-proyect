﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable] public struct TimeValues
{
    public GameObject bonusIcon, alertPrefab, timerPrefab;
    [Range(0, 60)] public int timeInMinutes;
}
[Serializable] public struct TimeParameters
{
    public Button bonus;
    public TextMeshProUGUI time;
    public DateTime newDate;
}
public class SetTimer : MonoBehaviour
{
    [SerializeField] TimeValues time;
    [SerializeField] TimeParameters values;

    private User_Interface ui;
    private DateTime currentTime;
    [HideInInspector] public string timeLeft;

    private void Awake()
    {
        ui = GetComponent<User_Interface>();
        DateTime newTime = DateTime.Now.AddMinutes(time.timeInMinutes);
        DateTime setTime = DateTime.Parse(PlayerPrefs.GetString("date", newTime.ToString()));
        values.newDate = setTime;
    }
    private void Update()
    {
        currentTime = DateTime.Now;

        GameObject bonusUI = (currentTime < values.newDate) ? time.timerPrefab : time.alertPrefab;
        SetBonusUI(bonusUI);

        timeLeft = (currentTime <= values.newDate) ? CountDown(currentTime) : "00:00";
        values.time.text = timeLeft;
    }
    private string CountDown(DateTime now)
    {
        int targetSec = values.newDate.Minute * 60 + values.newDate.Second;
        int nowSec = now.Minute * 60 + now.Second;
        int diff = targetSec - nowSec;

        var ts = TimeSpan.FromSeconds(diff);
        string time = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds); ;

        return time;
    }
    private void SetBonusUI(GameObject interfaz)
    {
        values.bonus.onClick.RemoveAllListeners();
        values.bonus.onClick.AddListener(() => ui.InstanceUI(interfaz));
    }
    public void SetNewTime()
    {
        values.newDate = currentTime.AddMinutes(time.timeInMinutes);
        PlayerPrefs.SetString("date", values.newDate.ToString());
    }
}