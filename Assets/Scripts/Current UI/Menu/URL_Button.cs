﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
public class URL_Button : MonoBehaviour
{
    public string _url;

    private void Awake()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((data) => { ShowURL(_url); });
        GetComponent<EventTrigger>().triggers.Add(entry);
    }
    public void ShowURL(string _url)
    {
        if (_url != "") Application.OpenURL(_url); else Debug.LogWarning("URL not set");
    }
}