﻿using System;
using System.Collections;
using UnityEngine;
using TMPro;

[Serializable] public struct InputFields
{
    public string inputsType;
    public RectTransform[] inputs;
}
public class AuthManager : MonoBehaviour
{
    [SerializeField] InputFields[] inputFields;
    public float _separation, _animSpeed;
    public TextMeshProUGUI textButton;

    [HideInInspector] public bool acount;
    private bool change; 

    private void Awake()
    {
        SetInputFields(0);
        StartCoroutine(OpenInputFields(0));
    }
    private void SetInputFields(int active)
    {
        for (int i = 0; i < inputFields.Length; i++)
        {
            bool enable = (active == i) ? true : false;

            for (int j = 0; j < inputFields[i].inputs.Length; j++)
            {
                RectTransform rect = inputFields[i].inputs[j];
                rect.localPosition = Vector2.zero;
                rect.GetComponentInChildren<TextMeshProUGUI>().color = new Color(1, 1, 1, 0);
                rect.gameObject.SetActive(enable);
            }
        }
    }
    private IEnumerator NewInputFields(int num, bool enable)
    {
        change = false;

        for (int i = 0; i < inputFields[num].inputs.Length; i++)
        {
            int value = i;
            GameObject field = inputFields[num].inputs[i].gameObject; 
            RectTransform rect = inputFields[num].inputs[i];
            TextMeshProUGUI text = field.GetComponentInChildren<TextMeshProUGUI>();
            TMP_InputField content = field.GetComponent<TMP_InputField>();

            if (!field.activeSelf) field.SetActive(true); else content.text = "";
            TweenFields(field, rect, text, value, enable);
        }

        if(!enable) yield return new WaitUntil(predicate: () => change);
    }
    private IEnumerator OpenInputFields(int active)
    {
        int desable = (active == 0) ? 1 : 0;
        if (inputFields[desable].inputs[0].gameObject.activeSelf)
        {
            LeanTween.cancelAll();
            yield return StartCoroutine(NewInputFields(desable, false));
        }

        StartCoroutine(NewInputFields(active, true));
    }
    private void TweenFields(GameObject input, RectTransform rect, TextMeshProUGUI text, int count, bool create)
    {
        float pos = rect.localPosition.y; float end = (create) ? count * -_separation : 0; 
        float posDelay = (create) ? 0 : 0.25f;

        LTDescr position = LeanTween.value(input, pos, end, _animSpeed).setEase(LeanTweenType.easeInOutCirc);
        position.setOnComplete(() => { change = true; if (!create) input.SetActive(false); });
        position.setOnUpdate((float val) => rect.localPosition = Vector2.up * val);
        position.setDelay(posDelay);

        float alpha = text.color.a; float endAlpha = (create) ? 1 : 0; 
        float colorDelay = (create) ? 0.25f : 0;

        LTDescr textAlpha = LeanTween.value(input, alpha, endAlpha, _animSpeed / 2);
        textAlpha.setOnUpdate((float val) => text.color = new Color(1, 1, 1, val)).setDelay(colorDelay);
    }
    public void ChangeAcount()
    {
        if (change)
        {
            acount = !acount;
            int value = (acount) ? 1 : 0;
            StartCoroutine(OpenInputFields(value));
            textButton.text = (acount) ? "Register" : "Login";
        }
    }
}