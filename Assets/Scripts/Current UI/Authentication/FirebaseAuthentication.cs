﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Auth;
using TMPro;

[Serializable] public struct Inputs
{
    [Header("Fx")]
    public GameObject _fade;
    public GameObject _loading;
    [Header("Login")]
    public TMP_InputField email;
    public TMP_InputField password;
    [Header("Register")]
    public TMP_InputField newEmail;
    public TMP_InputField username;
    public TMP_InputField newPassword;
    public TMP_InputField passwordVerify;
}
public class FirebaseAuthentication : MonoBehaviour
{
    [SerializeField] Inputs inputs;

    [Header("Fire Base")]
    public FirebaseAuth auth;
    static public FirebaseUser user;
    private AuthManager _authManager;

    private void Awake()
    {
        _authManager = GetComponent<AuthManager>();
        Debug.Log(Application.persistentDataPath);
        Player.LoadData();
        if (Player.userId != null && Player.userId != "")
        { Player.SaveData(); SceneManager.LoadScene(1); }

        auth = FirebaseAuth.DefaultInstance;
    }
    private IEnumerator LogIn(string email, string password)
    {
        Loading loadProgress = Instantiate(inputs._loading, transform).GetComponent<Loading>();
        string message = "Failed to login";
        var logInTask = auth.SignInWithEmailAndPasswordAsync(email, password);

        yield return new WaitUntil(predicate: () => logInTask.IsCompleted);

        if (logInTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to login with {logInTask.Exception}");
            FirebaseException firebaseEx = logInTask.Exception.GetBaseException() as FirebaseException;
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

            switch (errorCode)
            {
                case AuthError.MissingEmail: message = "Missing Email"; break;
                case AuthError.MissingPassword: message = "Missing Password"; break;
                case AuthError.WrongPassword: message = "Wrong Password"; break;
                case AuthError.InvalidEmail: message = "Invalid Email"; break;
                case AuthError.UserNotFound: message = "Account does not exist"; break;
            }
        }
        else
        {
            user = logInTask.Result;
            Player.userId = user.UserId;
            Player.userName = user.DisplayName;
            yield return StartCoroutine(Player.ReadFirestoreData());
            Debug.LogFormat("User singed in successfully: {0} ({1})", user.DisplayName, user.Email);
            message = "Singed Successfully";
            Instantiate(inputs._fade, transform).GetComponent<Fade_Controller>()._sceneName = "Menu";
        }

        loadProgress._text = message;
    }
    private IEnumerator Register(string email, string userName, string password)
    {
        Loading loadProgress = Instantiate(inputs._loading, transform).GetComponent<Loading>();
        string message = "Failed to Register";

        if (userName == "") { message = "username is empty"; }
        else if (password != inputs.passwordVerify.text) { message = "password is incorrect"; }
        else
        {
            var RegisterTask = auth.CreateUserWithEmailAndPasswordAsync(email, password);
            yield return new WaitUntil(predicate: () => RegisterTask.IsCompleted);

            if (RegisterTask.Exception != null)
            {
                Debug.LogWarning(message: $"Failed to register task with {RegisterTask.Exception}");
                FirebaseException firebaseEx = RegisterTask.Exception.GetBaseException() as FirebaseException;
                AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

                switch (errorCode)
                {
                    case AuthError.MissingEmail: message = "Missing Email"; break;
                    case AuthError.MissingPassword: message = "Missing Password"; break;
                    case AuthError.WeakPassword: message = "Weak Password"; break;
                    case AuthError.EmailAlreadyInUse: message = "Email Already In Use"; break;
                }
            }
            else
            {
                user = RegisterTask.Result;

                if (user != null)
                {
                    UserProfile profile = new UserProfile { DisplayName = userName };
                    var ProfileTask = user.UpdateUserProfileAsync(profile);
                    yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

                    if (ProfileTask.Exception != null)
                    {
                        Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
                        FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
                    }
                    else
                    {
                        Player.userId = user.UserId;
                        Player.userName = user.DisplayName;
                        Player.email = email;
                        Player.unlock.Add(true);
                        Player.colors = new string[3];
                        Player.colors[0] = "#FFFFFF"; Player.colors[1] = "#FFFFFF"; Player.colors[2] = "#FFFFFF";

                        Player.SaveData();
                        message = "Register Successfully";
                        Instantiate(inputs._fade, transform).GetComponent<Fade_Controller>()._sceneName = "Menu";
                    }
                }
            }
        }

        loadProgress._text = message;
    }
    public void AuthenticationButton()
    {
        if (!_authManager.acount) StartCoroutine(LogIn(inputs.email.text, inputs.password.text));
        else StartCoroutine(Register(inputs.newEmail.text, inputs.username.text, inputs.newPassword.text));
    }
}