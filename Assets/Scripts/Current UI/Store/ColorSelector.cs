﻿using UnityEngine;
using UnityEngine.UI;

public class ColorSelector : MonoBehaviour
{
    public GameObject colorPicker;
    public Image[] icons;

    private ShipSelector selector;
    private ColorPicker picker;

    private void Awake()
    {
        selector = GetComponent<ShipSelector>();

        for (int i = 0; i < selector.mats.Length; i++)
        {
            Color partColor;

            if (Player.colors == null) return;

            if (ColorUtility.TryParseHtmlString(Player.colors[i], out partColor))
            {
                selector.mats[i].SetColor("_Color", partColor);
                icons[i].color = partColor;
            }
        }
    }
    private void Update()
    {
        if (picker == null) return;
        icons[picker.num].color = picker._mainColor;
        selector.mats[picker.num].SetColor("_Color", picker._mainColor);
    }
    public void ColorPickerPrefab(int num)
    {
        picker = Instantiate(colorPicker, transform.parent).GetComponentInChildren<ColorPicker>();
        picker.num = num;
    }
}