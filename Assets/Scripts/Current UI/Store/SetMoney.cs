﻿using UnityEngine;
using TMPro;

public class SetMoney : MonoBehaviour
{
    private TextMeshProUGUI _text;

    private void Awake()
    {
        _text = GetComponentInChildren<TextMeshProUGUI>();
        UpdateCoins();
    }
    public void UpdateCoins()
    {
        _text.SetText(Player.coins.ToString());
    }
}