﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.Mathematics;

[Serializable] public struct StoreValues
{
    [Header("Rockets")]
    public Transform shipView;
    public Transform container;
    public TextMeshProUGUI priceText;
    
    [Header("UI")]
    public SetMoney money;
    public TextMeshProUGUI mainButtonTxt;
    public string[] buttonText;
}
[Serializable] public struct Spacecraft 
{
    public GameObject fbx;
    public Sprite image;
    public int price;
}
public class ShipSelector : MonoBehaviour
{
    [SerializeField] StoreValues values;
    [SerializeField] Spacecraft[] rockets;

    public Material[] mats;
    private int currentSelect;

    private void Awake()
    {
        Transform container = values.container;
        RectTransform containerSize = container.GetComponent<RectTransform>();

        GameObject ItemPrefab = container.GetChild(0).gameObject;

        float itemSize = ItemPrefab.GetComponent<RectTransform>().sizeDelta.x;
        float spacing = container.GetComponent<HorizontalLayoutGroup>().spacing;
        float2 size = new float2(spacing, containerSize.sizeDelta.y);

        for (int i = 0; i < rockets.Length; i++)
        {
            int val = i;
            Button item = Instantiate(ItemPrefab, container).GetComponent<Button>();
            item.onClick.AddListener(() => ItemButton(val, rockets[val].fbx));

            item.transform.GetChild(0).GetComponent<Image>().sprite = rockets[i].image;
            size.x += itemSize + spacing;
        }

        containerSize.sizeDelta = size;
        Destroy(ItemPrefab);

        int num = Player.ship;
        ItemButton(num, rockets[num].fbx);
    }
    private void SetButton(bool active)
    {
        values.mainButtonTxt.text = active ? values.buttonText[0] : values.buttonText[1];
    }
    private void SetMaterials(Transform ship, int num, bool enable)
    {
        values.priceText.text = (!enable) ? "$" + rockets[num].price.ToString() : "";

        if (!enable) return;

        for (int i = 0; i < mats.Length; i++)
            ship.GetChild(i).GetComponent<MeshRenderer>().material = mats[i];
    }
    public void ItemButton(int num, GameObject spaceShip)
    {
        if (values.shipView.childCount > 0) Destroy(values.shipView.GetChild(0).gameObject);
        if (Player.unlock.Count <= num) Player.unlock.Add(false);

        bool enable = Player.unlock[num];
        SetButton(enable);
        Transform ship = Instantiate(spaceShip, values.shipView).transform;
        SetMaterials(ship, num, enable);
        currentSelect = num;
    }
    public void ApllyButton()
    {
        if (!Player.unlock[currentSelect])
        {
            if(Player.coins >= rockets[currentSelect].price)
            {
                Player.coins -= rockets[currentSelect].price;
                Player.unlock[currentSelect] = true;
                SaveSystem.SaveParameters();
                values.money.UpdateCoins();
                SetMaterials(values.shipView.GetChild(0), currentSelect, true);
                SetButton(true);
            }
            else
            {
                Debug.Log("no tienes dinero sufuciente");
            }
        }
        else
        {
            Player.ship = currentSelect;
            SaveSystem.SaveParameters();
        }
    }
}