﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class ColorPicker : MonoBehaviour
{
    [HideInInspector] public int num;
    public Color _mainColor = Color.red;
    public string _htmlColor = "#F00000";
    public InputAction touch;

    private Transform roundPicker;
    private Image color;
    private Camera cam;
    private Vector3 mousePos;
    private Transform _transform;

    private void Start()
    {
        _transform = transform;
        roundPicker = _transform.GetChild(0);
        color = _transform.GetChild(1).GetComponent<Image>();

        cam = Camera.main;
        if(Player.colors != null) _htmlColor = Player.colors[num];
        SetDefaultColor();
    }
    private void SetDefaultColor()
    {
        Color html;
        if (ColorUtility.TryParseHtmlString(_htmlColor, out html)) _mainColor = html;

        float h, s, v;
        Color.RGBToHSV(_mainColor, out h, out s, out v);
        
        color.color = Color.HSVToRGB(h, 1, 0.9f);
        roundPicker.localEulerAngles = Vector3.forward * (h * 360f);
    }
    public void RoundColorSelector()
    {
        Ray ray = cam.ScreenPointToRay(touch.ReadValue<Vector2>());
        Plane xy = new Plane(Vector3.forward, _transform.position);
        float distance;
        xy.Raycast(ray, out distance);
        mousePos = ray.GetPoint(distance);
        
        float angle = Mathf.Atan2(_transform.position.y - mousePos.y, _transform.position.x - mousePos.x) 
            * Mathf.Rad2Deg; angle += 90;
        roundPicker.localEulerAngles = new Vector3(0, 0, angle);

        float hug = ((angle / 360) < 0) ? (angle / 360) + 1: angle / 360;
        _mainColor = Color.HSVToRGB(Mathf.Abs(hug), 1, 0.9f);
        color.color = _mainColor;
        _htmlColor = "#" + ColorUtility.ToHtmlStringRGB(_mainColor);
    }
    public void SaveColor()
    {
        Player.colors[num] = _htmlColor;
        SaveSystem.SaveParameters();
        GetComponentInParent<New_UI_Instance>().ChangeUI(0);
    }
    private void OnEnable()
    {
        touch.Enable();
    }
    private void OnDisable()
    {
        touch.Disable();
    }
}