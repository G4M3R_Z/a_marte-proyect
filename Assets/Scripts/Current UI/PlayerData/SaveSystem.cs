﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SaveParameters()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.data";
        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData();
        formatter.Serialize(stream, data);
        stream.Close();

        Debug.Log("file saved succesfully in " + path);
    }
    public static PlayerData LoadParameters()
    {
        string path = Application.persistentDataPath + "/player.data";
        
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            Debug.Log("file loaded succesfully from " + path);
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogWarning("save file not found in " + path);
            SaveParameters();
            return null;
        }
    }
}