﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Firestore;
using Firebase.Extensions;

public class Player : MonoBehaviour
{
    static public string userId, userName, email;
    static public int progress, coins, ship;
    static public List<bool> unlock = new List<bool>();
    static public string[] colors = new string[3];

    static public void SaveData()
    {
        SaveSystem.SaveParameters();

        Dictionary<string, object> newData = new Dictionary<string, object>
        {
            { "Username", userName},
            { "Email", email },
            { "Progress", progress},
            { "Coins", coins },
            { "Nave", ship},
            { "Unlocked", unlock },
            { "Colors", colors }
        };
        SaveFirestoreData(newData);
    }
    static public void LoadData()
    {
        PlayerData data = SaveSystem.LoadParameters();
        
        if(data != null)
        {
            userId = data.userId;
            userName = data.userName;
            email = data.email;

            ship = data.currentShip;
            coins = data.totalCoins;
            progress = data.progress;

            unlock = new List<bool>(data.unlocked);
            colors = data.colors;
        }
    }
    static public void SaveFirestoreData(Dictionary<string, object> data)
    {
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;

        DocumentReference docRef = db.Collection("Users").Document(userId);
        docRef.SetAsync(data).ContinueWithOnMainThread(task => { Debug.Log("Added data to collection."); });
    }
    static public IEnumerator ReadFirestoreData()
    {
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        DocumentReference usersRef = db.Collection("Users").Document(userId);
        string wait = "";

        usersRef.GetSnapshotAsync().ContinueWithOnMainThread(task =>
        {
            do
            {
                Debug.Log("Trying to acces networl");

                DocumentSnapshot snapshot = task.Result;

                userName = snapshot.GetValue<string>("Username");
                email = snapshot.GetValue<string>("Email");
                ship = snapshot.GetValue<int>("Nave");
                coins = snapshot.GetValue<int>("Coins");
                progress = snapshot.GetValue<int>("Progress");
                unlock = snapshot.GetValue<List<bool>>("Unlocked");
                colors = snapshot.GetValue<string[]>("Colors");
            } 
            while (task.Exception != null);

            Debug.Log("Read all data from the users collection.");
            SaveSystem.SaveParameters();
            wait = "Login Succesfull";
        });

        yield return new WaitUntil(() => wait != "");
    }
}