﻿
[System.Serializable]
public class PlayerData
{
    public string userId, userName, email;
    public int progress, totalCoins, currentShip;
    public bool[] unlocked;
    public string[] colors;

    public PlayerData()
    {
        userId = Player.userId;
        userName = Player.userName;
        email = Player.email;

        currentShip = Player.ship;
        totalCoins = Player.coins;
        progress = Player.progress;

        unlocked = Player.unlock.ToArray();
        colors = Player.colors;
    }
}