﻿using UnityEngine;
using UnityEngine.UI;

public class TileBackground : MonoBehaviour
{
    private RawImage tile;
    private float value;
    static public float _speedMultiply;

    private void Awake()
    {
        _speedMultiply = 1;
        tile = GetComponentInChildren<RawImage>();
    }
    private void Update()
    {
        if (tile == null) return;

        value += (Time.deltaTime / 10) * _speedMultiply;
        tile.uvRect = new Rect(0, value, 1, 1);
    }
}