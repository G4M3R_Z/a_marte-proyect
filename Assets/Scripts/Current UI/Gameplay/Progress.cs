﻿using System;
using System.Text;
using UnityEngine;
using TMPro;

[Serializable] public struct ProgressUI
{
    public int defaultLength;

    public Transform player;
    public GameObject levelCuestion;

    public RectTransform bar;
    public Color emptyColor, fillColor;
    public GameObject defeatUI, bomb;
}
[Serializable] public struct CompleteUI
{
    public string mainText;
    public Color mainColor;
    public string[] infoText;
    public Color[] infoColor;
    [Range(500,1000)] public float size;
}
public class Progress : MonoBehaviour
{
    [SerializeField] ProgressUI progressUI;
    [SerializeField] CompleteUI[] gameUI;

    private float score, size;
    private RectTransform colorBar;
    
    public static float _levelSize;
    public static bool _death, _pause;

    private void Awake()
    {
        size = progressUI.bar.rect.width;
        colorBar = progressUI.bar.GetChild(0).GetComponent<RectTransform>();

        _death = _pause = false;
        if (_levelSize == 0) _levelSize = progressUI.defaultLength;
    }
    private void Update()
    {
        if (_death || _pause) return;

        float add = Time.deltaTime * (size / _levelSize) * TileBackground._speedMultiply;
        score = Mathf.Clamp(score += add, 0, size);
        colorBar.sizeDelta = new Vector2(score, colorBar.sizeDelta.y);

        if (!_death && score == size) { _death = true; Instantiate(progressUI.levelCuestion); }
    }
    public void ScoreUI()
    {
        if (progressUI.defeatUI == null) return;
        RectTransform prefab = Instantiate(progressUI.defeatUI, transform).GetComponent<RectTransform>();
        TextMeshProUGUI mainText = prefab.GetChild(0).GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI infoText = prefab.GetChild(1).GetComponent<TextMeshProUGUI>();

        int index = (score < size) ? 0 : 1;

        prefab.sizeDelta = new Vector2(700, gameUI[index].size);

        #region Main Text
        string mainColor = ColorUtility.ToHtmlStringRGB(gameUI[index].mainColor);
        mainText.SetText(SetColorText(mainColor, gameUI[index].mainText));
        #endregion
        #region Info Text
        StringBuilder finalInfo = new StringBuilder();
        string[] info = gameUI[index].infoText;
        Color[] color = gameUI[index].infoColor;

        for (int i = 0; i < info.Length; i++)
        {
            string textColor = (color.Length > i) ? ColorUtility.ToHtmlStringRGB(color[i]) : "FFFFFF";
            info[i] = SetColorText(textColor, info[i]);
            finalInfo.Append(info[i] + "\n");

            /*finalInfo.Append((i == 0) ? score.ToString() : Player.highScore.ToString());
            finalInfo.Append((info.Length - 1 != i) ? "m\n\n" : "m");*/
        }

        infoText.text = finalInfo.ToString();
        #endregion
    }
    private string SetColorText(string html, string text)
    {
        StringBuilder color = new StringBuilder();
        color.Append("<color=#"); color.Append(html); color.Append(">");
        color.Append(text); color.Append("</color>");

        string finalColor = color.ToString();
        return finalColor;
    }
}