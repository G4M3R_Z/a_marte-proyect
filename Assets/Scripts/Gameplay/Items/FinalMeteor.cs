using System;
using System.Collections;
using UnityEngine;

[Serializable] public struct FinalLevel
{
    public Vector3 meteorPos, playerPos;
    public GameObject bombItem;
    public GameObject bonusSelector;
    public GameObject destroyParticles;
    public GameObject fade;
}
public class FinalMeteor : MonoBehaviour
{
    [HideInInspector] public bool _destroy;
    [SerializeField] FinalLevel values;
    private Transform canvas, player;
    private User_Interface ui;

    private void Awake()
    {
        canvas = GameObject.FindGameObjectWithTag("Canvas").transform;
        player = GameObject.FindGameObjectWithTag("Player").transform;

        ui = canvas.GetComponent<User_Interface>();
        StartCoroutine(FinalAnimation());
    }
    IEnumerator FinalAnimation()
    {
        #region Destroy All Enemys

        ui.SetCurrentUI(0);
        Bomb item = Instantiate(values.bombItem).GetComponent<Bomb>();
        item.delay = item.duration = 0.1f;

        #endregion
        #region Set Elements to Position

        float time = 1;
        Vector3 PPos = player.position;
        Vector3 MPos = transform.position;
        player.GetComponent<CharacterMovement>().enabled = false;

        while (time > 0)
        {
            player.position = values.playerPos + ((PPos - values.playerPos) * time);
            transform.position = values.meteorPos + ((MPos - values.meteorPos) * time);
            yield return new WaitForEndOfFrame();
            time -= Time.deltaTime / 5;
        }

        #endregion
        #region Instance Question
        Instantiate(values.bonusSelector, canvas);
        #endregion
        #region DestroyMeteor
        yield return new WaitUntil(() => _destroy);

        //sonido de destruir
        Instantiate(values.destroyParticles, transform.position, Quaternion.identity);
        GetComponent<MeshRenderer>().enabled = false;

        Vector2 playerPos = Vector2.zero;
        while (playerPos.y < 7)
        {
            playerPos = player.position;
            playerPos.y += Time.deltaTime * 5;
            player.position = playerPos;
            yield return new WaitForEndOfFrame();
        }
        #endregion
        #region Save Parameters
        
        Player.progress += 1; Player.coins += 100;
        SaveSystem.SaveParameters();

        Instantiate(values.fade, canvas).GetComponent<Fade_Controller>()._sceneName = "Menu";
        #endregion
    }
}