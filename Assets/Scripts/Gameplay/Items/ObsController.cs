﻿using System;
using UnityEngine;

[Serializable] public struct ItemsDrop
{
    [Range(1, 5)] public int life;
    public GameObject obsParticle;

    [Tooltip("Only needed if asteroid drops an ability")]
    public GameObject[] items;
}
public class ObsController : MonoBehaviour
{
    [SerializeField] ItemsDrop item;
    private bool drop;
    private int life;

    private void Awake()
    {
        life = item.life;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
        {
            Destroy(other.gameObject);

            if (life > 1) life--;
            else
            {
                Vector3 pos = transform.position;
                Quaternion rot = Quaternion.identity;

                if (item.items.Length != 0 && !drop)
                {
                    GameObject Item = item.items[UnityEngine.Random.Range(0, item.items.Length)];
                    Instantiate(Item, pos, rot);
                    drop = true;
                }

                if(item.obsParticle != null) Instantiate(item.obsParticle, pos, rot);
                Destroy(gameObject);
            }
        }
    }
}