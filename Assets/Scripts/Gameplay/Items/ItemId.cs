﻿using UnityEngine;

public class ItemId : MonoBehaviour
{
    [Range(0, 3)] public int id;
}