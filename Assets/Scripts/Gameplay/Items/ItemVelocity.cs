﻿using System;
using UnityEngine;

[Serializable] public struct ItemValues
{
    public bool up;
    [Range(0, 15)] public float speed;
}

[RequireComponent(typeof(Rigidbody))]
public class ItemVelocity : MonoBehaviour
{
    [SerializeField] ItemValues item;
    [Range(0, 10)] public float destroyPos;

    private Rigidbody rgb;
    private Vector3 direction;

    private void Awake()
    {
        rgb = GetComponent<Rigidbody>(); rgb.useGravity = false;
        direction = (item.up) ? Vector3.up : Vector3.down;
    }
    private void Update()
    {
        Vector3 movement = (!Progress._pause) ? direction * item.speed * TileBackground._speedMultiply : Vector3.zero;
        rgb.velocity = movement;

        bool destroyItem = (item.up) ? rgb.position.y > destroyPos : rgb.position.y < -destroyPos;
        if (destroyItem) Destroy(this.gameObject);
    }
}