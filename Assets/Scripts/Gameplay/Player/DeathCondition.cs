using UnityEngine;

public class DeathCondition : MonoBehaviour
{
    [HideInInspector] public bool shield;
    public GameObject destroyParticle;
    private Progress score;

    private void Awake()
    {
        score = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Progress>();
    }
    private void OnTriggerEnter(Collider other)
    {
        ObsController obs = other.GetComponent<ObsController>();
        if (obs == null) return;

        if (shield)
        { 
            Instantiate(destroyParticle, other.transform.position, Quaternion.identity);
            Destroy(other.gameObject);
            shield = false;
            return; 
        }

        Progress._death = true; score.ScoreUI();

        Instantiate(destroyParticle, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
}