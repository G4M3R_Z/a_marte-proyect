﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

[Serializable] public struct Movement
{
    [Range(0, 5)] public float speed;
    [Range(0, 1)] public float edgeDistance;
    public InputAction touchPos;
}
[Serializable] public struct MovementVariable
{
    public Camera cam;
    public Vector2 camSize;
    
    public Transform obj;
    public Vector3 position;
}
public class CharacterMovement : MonoBehaviour
{
    [SerializeField] Movement moves;
    MovementVariable v;

    private void Awake()
    {
        v.cam = Camera.main;
        v.obj = transform;

        v.camSize = v.cam.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        v.camSize.x -= moves.edgeDistance;
        v.camSize.y -= moves.edgeDistance;

        v.position = v.obj.position;
        moves.touchPos.performed += ctx => Position(ctx);
    }
    private void Update()
    {
        if(Progress._pause) return;
        v.obj.position = Movement(v.position);
    }
    private void Position(InputAction.CallbackContext ctx)
    {
        Vector3 touchPos = v.cam.ScreenToWorldPoint(ctx.ReadValue<Vector2>());
        if (Progress._pause || TouchOverUI.IsOverUI(v.cam, touchPos)) return;
        v.position = touchPos;
    }
    private Vector3 Movement(Vector3 pos)
    {
        float deltaTime = Time.deltaTime;
        Vector3 player = v.obj.position;

        player.x = Mathf.Lerp(player.x, pos.x, deltaTime * moves.speed);
        player.x = Mathf.Clamp(player.x, -v.camSize.x, v.camSize.x);

        player.y = Mathf.Lerp(player.y, pos.y, deltaTime * moves.speed);
        player.y = Mathf.Clamp(player.y, -v.camSize.y, v.camSize.y);

        player.z = 0;

        return player;
    }
    private void OnEnable()
    {
        moves.touchPos.Enable();
    }
    private void OnDisable()
    {
        moves.touchPos.Disable();
    }
}