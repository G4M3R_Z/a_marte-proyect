using System;
using UnityEngine;

[Serializable] public struct Skills
{
    public string skillName;
    public GameObject skillPrefab, stopEffect;
    [Range(0, 10)] public float minValue, maxValue;
    public bool needTime;
}
public class SkillManager : MonoBehaviour
{
    [SerializeField] Skills[] skills;
    [HideInInspector] public int[] tweenId;
    [HideInInspector] public float[] values;
    private GameObject[] effects;

    private void Awake()
    {
        tweenId = new int[skills.Length];
        effects = new GameObject[skills.Length];

        values = new float[skills.Length];
        for (int i = 0; i < values.Length; i++) values[i] = skills[i].minValue;
    }
    private void SetSkill(int i)
    {
        if (skills[i].skillPrefab == null) { Debug.LogWarning("Skill not Set"); return; }
        if (effects[i] == null) effects[i] = Instantiate(skills[i].skillPrefab, transform);

        if (skills[i].needTime) return;
        if (tweenId[i] != 0) 
        { LeanTween.cancel(tweenId[i]); tweenId[i] = 0; }

        float max = skills[i].maxValue;
        float min = skills[i].minValue;

        LTDescr tween = LeanTween.value(effects[i], max, min, 5f);
        tweenId[i] = tween.id;

        tween.setOnUpdate((float val) => values[i] = val);
        tween.setOnComplete(() => 
        {
            if (skills[i].stopEffect != null) 
                Instantiate(skills[i].stopEffect, transform.position, Quaternion.identity);
            Destroy(effects[i]); tweenId[i] = 0;
        });
    }
    private void OnTriggerEnter(Collider other)
    {
        ItemId item = other.GetComponent<ItemId>();
        if (item != null) { SetSkill(item.id); Destroy(other.gameObject); }
    }
}