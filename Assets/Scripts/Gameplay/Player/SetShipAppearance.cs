﻿using System;
using UnityEngine;

[Serializable] public struct Appearance
{
    public GameObject[] graphic;
    public Material[] mats;
    public GameObject burstParticle;
}
public class SetShipAppearance : MonoBehaviour
{
    [SerializeField] Appearance look;
    private void Awake()
    {
        if (transform.childCount > 0) Destroy(transform.GetChild(0).gameObject);

        int value = Player.ship;
        Transform ship = Instantiate(look.graphic[value], transform).transform;
        ship.rotation = Quaternion.Euler(0, 180, 0);

        if (Player.colors == null) return;

        for (int i = 0; i < ship.childCount; i++)
        {
            Color partColor;

            if (ColorUtility.TryParseHtmlString(Player.colors[i], out partColor))
                look.mats[i].SetColor("_Color", partColor);

            ship.GetChild(i).GetComponent<MeshRenderer>().material = look.mats[i];
        }

        Instantiate(this.look.burstParticle, transform);
        Destroy(this);
    }
}