﻿using System;
using UnityEngine;

public class SkillsManager : MonoBehaviour
{
    [Serializable] public struct SkillParameters
    {
        public string name;
        [Range(0, 1)] public float delay;
        [Range(0, 10)] public float minValue, maxValue;
        public GameObject item, particleIn, particleOut;
    }
    [SerializeField] SkillParameters[] skill;

    private GameObject[] items, particles;
    [HideInInspector] public float[] delay, value, min;
    [HideInInspector] public bool[] enable;

    void Awake()
    {
        #region Time Delay Arrays
        value = new float[skill.Length];
        min = new float[skill.Length];
        enable = new bool[skill.Length];

        for (int i = 0; i < skill.Length; i++) value[i] = min[i] = skill[i].minValue;
        #endregion
        items = new GameObject[skill.Length];
        particles = new GameObject[skill.Length];
        
        delay = new float[skill.Length];
        for (int i = 0; i < skill.Length; i++) { delay[i] = skill[i].delay; }
    }
    private void Update()
    {
        if(Progress._pause) return;
        for (int i = 0; i < skill.Length; i++) { int num = i; SetTime(num); SkillVisualEffect(i); }
    }
    private void OnTriggerEnter(Collider other)
    {
        ItemId item = other.GetComponent<ItemId>();
        if (item == null) return;
        value[item.id] = skill[item.id].maxValue;
        Destroy(other.gameObject);
    }
    private void SkillVisualEffect(int i)
    {
        if (value[i] == min[i])
        {
            if (items[i] != null) Destroy(items[i]);
            if (particles[i] != null)
            { 
                Destroy(particles[i]);
                
                if (skill[i].particleOut != null)
                    Instantiate(skill[i].particleOut, transform.position, Quaternion.identity);
            }
            return;
        }

        SetVisual(ref items, skill[i].item, i);
        SetVisual(ref particles, skill[i].particleIn, i);
    }
    private void SetVisual(ref GameObject[] parts, GameObject visual, int index)
    {
        if (parts[index] == null && visual != null)
            parts[index] = Instantiate(visual, transform);
    }
    private void SetTime(int i)
    {
        enable[i] = (value[i] <= min[i]) ? false : true;
        if (enable[i]) value[i] -= Time.deltaTime; else value[i] = min[i];
    }
}