﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Header("Parameters")]
    [Range(0, 10)] public float _obsLimit;
    [Range(0, 10)] public float _areaHorizontal, _areaVertical;
    [Range(0, 5)] public float firstDelay;

    [Header("Obstacles")]
    public List<GameObject> _obsPrefab;
    public GameObject _particleFx;

    [HideInInspector] public bool _spawn;
    private Transform _parent;

    private void Awake()
    {
        _spawn = true;
        _parent = transform.GetChild(0);
        StartCoroutine(Spawn());
    }
    private IEnumerator Spawn()
    {
        yield return new WaitForSeconds(firstDelay);

        do
        {
            yield return new WaitUntil(() => !Progress._pause && _spawn && !Progress._death);

            //Instanciar Obstaculo
            Transform Obs = Instantiate(_obsPrefab[Random.Range(0, _obsPrefab.Count)], _parent).transform;
            Obs.GetComponent<ItemVelocity>().destroyPos = _obsLimit;

            //Set Random Pos & Rot
            float xPos = Random.Range(-_areaHorizontal / 2, _areaHorizontal / 2);
            float yPos = Random.Range(-_areaVertical / 2, _areaVertical / 2);
            Obs.localPosition = new Vector2(xPos, yPos);
            Obs.localEulerAngles = new Vector3(0, 0, Random.Range(0, 359));

            yield return new WaitForSeconds(Random.Range(0.3f, 0.5f));
        } 
        while (!Progress._death);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 size = new Vector3(_areaHorizontal, _areaVertical, 0);
        Gizmos.DrawWireCube(transform.GetChild(0).position, size);
        Gizmos.DrawLine(new Vector3(-2.5f, -_obsLimit, 0), new Vector3(2.5f, -_obsLimit, 0));
    }
}