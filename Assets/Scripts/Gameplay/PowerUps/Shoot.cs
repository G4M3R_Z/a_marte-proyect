using System;
using System.Collections;
using Unity.Mathematics;
using UnityEngine;

[Serializable] public struct ShootValues
{
    public GameObject bullet;
    public Transform parent;
    [Range(0, 10)] public float limitArea;
    [Range(0, 1)] public float separation, height, delay;
}
public class Shoot : MonoBehaviour
{
    [SerializeField] ShootValues values;
    private InputManager input;
    private bool shoot;
    public bool active;

    private void Awake()
    {
        input = new InputManager();
        input.Gameplay.Touch.performed += ctx => shoot = !shoot;
        StartCoroutine(ShootSkill());
    }
    private void OnEnable()
    {
        input.Enable();
    }
    private void OnDisable()
    {
        input.Disable();
    }
    private IEnumerator ShootSkill()
    {
        while (!Progress._death)
        {
            yield return new WaitUntil(() => !Progress._pause && shoot);

            for (int i = -1; i <= 1; i++)
            {
                float3 pos = transform.position;
                pos.y += values.height;
                pos.x += values.separation * i;

                if (values.bullet != null)
                {
                    if (!active) { pos.x = transform.position.x; i = 1; }
                    GameObject item = Instantiate(values.bullet, pos, Quaternion.identity, values.parent);
                    item.GetComponent<ItemVelocity>().destroyPos = values.limitArea;
                }
            }

            yield return new WaitForSeconds(values.delay);
        }
    }
}