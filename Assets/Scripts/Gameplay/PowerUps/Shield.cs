using UnityEngine;

public class Shield : MonoBehaviour
{
    public GameObject destryParticles;

    private ItemId id;
    private SkillManager skills;
    private DeathCondition death;

    private void Start()
    {
        id = GetComponent<ItemId>();
        skills = GetComponentInParent<SkillManager>();
        death = GetComponentInParent<DeathCondition>();
        death.shield = true;
    }
    private void Update()
    {
        if(death.shield == false)
        {
            LeanTween.cancel(skills.tweenId[id.id]);
            skills.values[id.id] = 0; skills.tweenId[id.id] = 0;
            Instantiate(destryParticles, transform);
            Destroy(this.gameObject);
        }
    }
    private void OnDisable()
    {
        death.shield = false;
    }
}