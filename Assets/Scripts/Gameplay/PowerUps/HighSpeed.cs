using UnityEngine;

public class HighSpeed : MonoBehaviour
{
    private int id;
    private SkillManager sk;
    private DeathCondition death;

    private void Start()
    {
        id = GetComponent<ItemId>().id;
        sk = GetComponentInParent<SkillManager>();
        death = GetComponentInParent<DeathCondition>();
    }
    private void Update()
    {
        death.shield = true;
        TileBackground._speedMultiply = sk.values[id];
    }
    private void OnDisable()
    {
        death.shield = false;
    }
}