using UnityEngine;

public class AddShoot : MonoBehaviour
{
    private Shoot shoot;

    private void Start()
    {
        shoot = transform.parent.GetComponentInChildren<Shoot>();
        shoot.active = true;
    }
    private void OnDisable()
    {
        if(shoot != null) shoot.active = false;
    }
}