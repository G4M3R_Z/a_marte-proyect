using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [Range(0, 5)]
    public float delay, duration;
    public GameObject particleFx;
    private Spawner spawn;
    private Transform obstacle;

    private void Start()
    {
        Transform Spawner = GameObject.FindGameObjectWithTag("Respawn").transform;
        spawn = Spawner.GetComponent<Spawner>();
        obstacle = Spawner.GetChild(0);

        StartCoroutine(DestroyAllObject(delay, duration));
    }
    private IEnumerator DestroyAllObject(float delay, float time)
    {
        spawn._spawn = false;

        List<GameObject> obj = new List<GameObject>();
        for (int i = 0; i < obstacle.childCount; i++) obj.Add(obstacle.GetChild(i).gameObject);

        float timeLeft = time;

        foreach (GameObject item in obj)
        {
            if (item != null) 
            { 
                if(particleFx != null) Instantiate(particleFx, item.transform.position, Quaternion.identity); 
                Destroy(item); 
            }
            
            yield return new WaitForSeconds(delay);
            if(timeLeft - delay > 0) timeLeft -= delay;
        }

        yield return new WaitForSeconds(timeLeft);
        spawn._spawn = true;
        Destroy(this.gameObject);
    }
}