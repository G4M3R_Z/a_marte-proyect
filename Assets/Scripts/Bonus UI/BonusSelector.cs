﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable] public struct RouletteValues
{
    public Sprite icon;
    [Range(0, 0.5f)] public float rouletteDelay;
    public LeanTweenType inAnim, outAnim; 
    public GameObject bonus;
}
[Serializable] public struct BonusTypes
{
    public string type;
    public Scriptable_Question[] questions;
}
public class BonusSelector : MonoBehaviour
{
    [SerializeField] RouletteValues values;
    [SerializeField] BonusTypes[] _bonusType;

    private RectTransform selector;
    private User_Interface ui;

    private void Awake()
    {
        ui = GetComponentInParent<User_Interface>();

        transform.localScale = Vector3.zero;        
        ui.TweenAnimationUI(this.gameObject, Vector3.one, 0.5f, values.inAnim, false);

        GameObject listType = transform.GetChild(0).gameObject;
        for (int i = 0; i < _bonusType.Length; i++)
        {
            Transform type = Instantiate(listType, transform).transform;
            TextMeshProUGUI text = type.GetComponent<TextMeshProUGUI>();
            text.text = _bonusType[i].type;

            if (i == 0)
            {
                selector = Instantiate(new GameObject("selector"), type).AddComponent<RectTransform>();
                selector.sizeDelta = Vector2.zero;
                selector.anchorMin = Vector2.zero;
                selector.anchorMax = Vector2.one;

                Image sprite = selector.gameObject.AddComponent<Image>();
                sprite.raycastTarget = sprite.maskable = false;
                sprite.color = new Color(1, 1, 1, 0.5f);
                sprite.sprite = values.icon;
            }
        }
        DestroyImmediate(listType);

        StartCoroutine(RandomBonus());
    }
    private IEnumerator RandomBonus()
    {
        yield return new WaitUntil(() => transform.localScale == Vector3.one);
        int randomType = UnityEngine.Random.Range(40, 50);
        int select = 0;

        for (int i = 0; i < randomType; i++)
        {
            selector.SetParent(transform.GetChild(select));
            selector.localPosition = Vector3.zero;

            //play music sound
            select = (select >= transform.childCount - 1) ? 0 : select += 1;
            yield return new WaitForSeconds(values.rouletteDelay);
        }

        yield return new WaitForSeconds(1f);

        ui.TweenAnimationUI(this.gameObject, Vector3.zero, 0.2f, values.outAnim, true);
        
        int random = UnityEngine.Random.Range(0, _bonusType[select].questions.Length);
        Bonus bonus = Instantiate(values.bonus, transform.parent).GetComponent<Bonus>();
        bonus.question = _bonusType[select].questions[random];
    }
}