using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;

[Serializable] public struct Email
{
    public string url;
    public string adminEmail;
}
public class Bonus : MonoBehaviour
{
    [SerializeField] Email email;
    [HideInInspector] public Scriptable_Question question;
    private User_Interface ui;
    private FinalMeteor meteor;

    private void Start()
    {
        ui = GameObject.FindGameObjectWithTag("Canvas").GetComponent<User_Interface>();
        meteor = GameObject.FindGameObjectWithTag("Finish").GetComponent<FinalMeteor>();

        transform.localScale = Vector3.zero;
        ui.TweenAnimationUI(this.gameObject, Vector3.one, 0.5f, LeanTweenType.easeOutQuart, false);

        transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = question._question;

        List<int> secuence = new List<int>();

        for (int i = 1; i < transform.childCount; i++)
        {
            int random = UnityEngine.Random.Range(0, question._answers.Count);
            if (secuence.Contains(random)) i--;
            else
            {
                secuence.Add(random);
                Button button = transform.GetChild(i).GetComponent<Button>();
                button.onClick.AddListener(() => SendMessage(question._answers[random], question._question));
                button.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = question._answers[random];
            }
        }
    }
    private void SendMessage(string answer, string question)
    {
        StartCoroutine(SendEmail(email.adminEmail, email.url, question, answer));
        ui.TweenAnimationUI(this.gameObject, Vector3.zero, 0.5f, LeanTweenType.easeInSine, true);
        meteor._destroy = true;
    }
    static IEnumerator SendEmail(string email, string url, string question, string answer)
    {
        if((Player.userName != "" && Player.userName != null) && (Player.email != "" && Player.email != null))
        {
            WWWForm form = new WWWForm();
            form.AddField("To", email);
            form.AddField("From", Player.email);

            form.AddField("User", Player.userName);
            form.AddField("Question", question);
            form.AddField("Answer", answer);

            UnityWebRequest request = UnityWebRequest.Post(url, form);
            yield return request.SendWebRequest();
            Debug.Log("Email send: " + request.result);
        }
    }
}