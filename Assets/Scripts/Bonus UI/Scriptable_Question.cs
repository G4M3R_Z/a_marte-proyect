﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Question_Data", menuName = "Question Type", order = 2)]
public class Scriptable_Question : ScriptableObject
{
    [TextArea] public string _question;
    public List<string> _answers;
}