﻿using System;
using System.Text;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

[Serializable] struct EmailForAnswers
{
    [Header("Admin Email")]
    public string email;
    public GameObject load;
}
public class EmailSender : MonoBehaviour
{
    [SerializeField] EmailForAnswers email;
    private FinalMeteor meteor;
    private User_Interface ui;

    private void Awake()
    {
        meteor = GameObject.FindGameObjectWithTag("Finish").GetComponent<FinalMeteor>();
        ui = GameObject.FindGameObjectWithTag("Canvas").GetComponent<User_Interface>();
    }
    public void SendEmail(string question, string answer)
    {
        StartCoroutine(Email(question, answer));
    }
    public IEnumerator Email(string question, string answer)
    {
        /*if(Player.email != null)
        {*/
            #region Email Head
            StringBuilder head = new StringBuilder();
            head.Append("el usuario: ");
            head.Append(Player.userName);
            head.Append(" a respondido una pregnuta");
            #endregion
            #region Email Body
            StringBuilder body = new StringBuilder();
            body.Append("Ante la pregunta\n");
            body.Append(question + "\n\n");
            body.Append("el usuario de correo ");
            body.Append(Player.email);
            body.Append(" respondio ");
            body.Append(answer);
            #endregion

            WWWForm form = new WWWForm();
            form.AddField("name", head.ToString());
            form.AddField("fromEmail", "pablo.0698@hotmail.com");
            form.AddField("toEmail", email.email);
            form.AddField("message", body.ToString());

            UnityWebRequest myWr = UnityWebRequest.Post("https://email-configuration.000webhostapp.com/emailer.php", form);
            Loading load = Instantiate(email.load, transform.parent).GetComponent<Loading>();
            yield return myWr.SendWebRequest();
            load._text = myWr.result.ToString();
        /*}*/

        ui.TweenAnimationUI(this.gameObject, Vector3.zero, 0.5f, LeanTweenType.easeInSine, true);
        meteor._destroy = true;
    }
}