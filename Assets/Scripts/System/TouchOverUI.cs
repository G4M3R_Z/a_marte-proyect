﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchOverUI : MonoBehaviour
{
    static public bool IsOverUI(Camera cam, Vector3 touchPos)
    {
        PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = cam.WorldToScreenPoint(touchPos);

        List<RaycastResult> raycastResultList = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerEventData, raycastResultList);

        for (int i = 0; i < raycastResultList.Count; i++)
        {
            if(raycastResultList[i].gameObject.GetComponent<UIClickThrough>() != null)
            {
                raycastResultList.RemoveAt(i);
                i--;
            }
        }
        return raycastResultList.Count > 0;
    }
}