﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[Serializable] public struct LoadingValues
{
    [Range(0, 1)] public float minLenth, maxLenth;
    public Image loadImage, logMessage;
    public TextMeshProUGUI _infoText;
}
public class Loading : MonoBehaviour
{
    public string _text;
    [SerializeField] LoadingValues loading;

    private Transform circle, message;
    private bool right;

    private void Awake()
    {
        circle = transform.GetChild(0); message = transform.GetChild(1);
        SetTextInfo(_text);
    }
    private void FixedUpdate()
    {
        if (circle == null) return;

        float amount = loading.loadImage.fillAmount;

        if (amount >= loading.maxLenth) right = false;
        else if (amount <= loading.minLenth) right = true;

        float speed = right ? 10 : 12;
        int dir = right ? 1 : -1;

        amount += Time.deltaTime * dir;
        loading.loadImage.fillAmount = amount;
        circle.localEulerAngles += Vector3.forward * speed;

        if (_text != "") 
        { GetComponent<Image>().enabled = false; SetTextInfo(_text); Destroy(circle.gameObject); }
    }
    public void SetTextInfo(string textInfo)
    {
        loading._infoText.SetText(textInfo);
        loading._infoText.ForceMeshUpdate();

        Vector2 textSize = (textInfo != "") ? loading._infoText.GetRenderedValues(false) : Vector2.zero;
        Vector2 paddingSize = (textInfo != "") ? new Vector2(120, 60) : Vector2.zero;

        message.GetComponent<RectTransform>().sizeDelta = textSize + paddingSize;
        
        if(textInfo != "")
        {
            LTDescr fade = LeanTween.value(gameObject, 1, 0, 1f).setDelay(2f);
            fade.setOnUpdate((float val) => 
            {
                Color textColor = loading._infoText.color;
                Color color = loading.logMessage.color;
                textColor.a = val;
                color.a = val;
                loading._infoText.color = textColor;
                loading.logMessage.color = color; 
            });
            fade.setDestroyOnComplete(true);
        }
    }
}