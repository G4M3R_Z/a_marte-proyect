﻿using System;
using UnityEngine;

[Serializable] public struct Audio
{
    public AudioSource mainVolume;
    public AudioSource[] effectVolume;
}
public class Audio_Manager : MonoBehaviour
{
    [SerializeField] Audio sound;
    static public float _mainVolume = 0.5f, _effectVolume = 0.5f;
    static public bool _mute = false;

    private void Awake()
    {
        SetVolumeValues();
    }
    private void Update()
    {
        if (sound.mainVolume == null || sound.effectVolume.Length == 0) return;
        if (_mainVolume == sound.mainVolume.volume && _effectVolume == sound.effectVolume[0].volume) return;
        SetVolumeValues();
    }
    private void SetVolumeValues()
    {
        if(sound.mainVolume != null)
        {
            sound.mainVolume.volume = _mainVolume;
            sound.mainVolume.mute = _mute;
        }

        foreach (AudioSource audio in sound.effectVolume)
        {
            audio.volume = _effectVolume;
            audio.mute = _mute;
        }
    }
}