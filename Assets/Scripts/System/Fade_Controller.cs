﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Serializable] public struct Values 
{ 
    [Range(0, 1)] public float time;
}
public class Fade_Controller : MonoBehaviour
{
    [SerializeField] Values value;
    [HideInInspector] public string _sceneName;
    private Image _image;

    private void Start()
    {
        bool newScene = (_sceneName != "") ? true : false;

        _image = GetComponent<Image>();
        int start = (newScene) ? 0 : 1; int end = (newScene) ? 1 : 0;
        _image.color = new Vector4(0, 0, 0, start);

        LTDescr fade = LeanTween.value(this.gameObject, start, end, value.time);
        fade.setOnUpdate((float val) => { _image.color = new Vector4(0, 0, 0, val); });
        fade.setOnComplete(() => 
        { 
            if(newScene) SceneManager.LoadScene(_sceneName);
            else Destroy(this.gameObject);
        });
    }
}