﻿using System;
using UnityEngine;

[Serializable] public struct Parameters
{
    [Range(0, 1)] public int firstUI;
    [Range(0, 1)] public float animTimeIn, animTimeOut;
    public LeanTweenType inAnimation, outAnimation;
    public GameObject fadePrefab;
}
[Serializable] public struct UI
{
    public string name;
    public GameObject[] currentUI;
}
public class User_Interface : MonoBehaviour
{
    [SerializeField] Parameters values;
    [SerializeField] UI[] userInterface;

    private bool _sceneChange;

    private void Awake()
    {
        SetFirstScale();
        SetCurrentUI(values.firstUI);
    }
    private void SetFirstScale()
    {
        for (int i = 0; i < userInterface.Length; i++)
        {
            for (int j = 0; j < userInterface[i].currentUI.Length; j++)
                userInterface[i].currentUI[j].transform.localScale = Vector3.zero;
        }
    }
    #region Animation Controller
    public void SetCurrentUI(int num)
    {
        for (int i = 0; i < userInterface.Length; i++)
        {
            for (int j = 0; j < userInterface[i].currentUI.Length; j++)
            {
                GameObject ui = userInterface[i].currentUI[j];
                if (i == num) TweenAnimationUI(ui, Vector3.one, values.animTimeIn, values.inAnimation,false);
                else TweenAnimationUI(ui, Vector3.zero, values.animTimeOut, values.outAnimation, false);
            }
        }
    }
    public void TweenAnimationUI(GameObject ui, Vector3 size, float time, LeanTweenType anim, bool destroy)
    {
        LeanTween.scale(ui, size, time).setEase(anim).setDestroyOnComplete(destroy);
    }
    #endregion
    #region UI Buttons Controller
    public void InstanceUI(GameObject uiPrefab)
    {
        Instantiate(uiPrefab, transform);
    }
    public void PauseButton(bool pause)
    {
        Progress._pause = pause;
    }
    public void ChangeScene(string sceneName)
    {
        if (_sceneChange || sceneName == "") return;

        Instantiate(values.fadePrefab, transform).GetComponent<Fade_Controller>()._sceneName = sceneName;
        _sceneChange = true;
    }
    #endregion
}