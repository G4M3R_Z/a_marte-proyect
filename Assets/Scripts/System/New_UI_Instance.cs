﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable] public struct InstanceUI
{
    [Header("Animation")]
    public LeanTweenType inAnim;
    [Range(0, 1)] public float inTime;
    public LeanTweenType outAnim;
    [Range(0, 1)] public float outTime;

    [Header("Parameters")]
    public List<GameObject> newInstance;
    [Range(0, 3)] public int currentUI, defaultUI;
    [Tooltip("If UI does have buttons, set value to true")]
    public bool useButtons;
}
public class New_UI_Instance : MonoBehaviour
{
    [SerializeField] InstanceUI ui;
    private InputManager input;
    private User_Interface _ui;
    private bool _newUI;

    private void Awake()
    {
        input = new InputManager();
        if(!ui.useButtons) input.UI.Tap.performed += ctx => ChangeUI(100);

        _ui = GetComponentInParent<User_Interface>();
        _ui.SetCurrentUI(ui.currentUI);

        transform.localScale = Vector3.zero;
        LeanTween.scale(gameObject, Vector3.one, ui.inTime).setEase(ui.inAnim);
    }
    public void ChangeUI(int instanceNum)
    {
        if (_newUI) return;
        LeanTween.scale(gameObject, Vector3.zero, ui.outTime).setEase(ui.outAnim).setDestroyOnComplete(true);
        
        if(ui.newInstance.Count > instanceNum)
        {
            if (ui.newInstance[instanceNum] != null)
                Instantiate(ui.newInstance[instanceNum], transform.parent);
            else
                _ui.SetCurrentUI(ui.defaultUI);
        }
        else _ui.SetCurrentUI(ui.defaultUI);

        _newUI = true;
    }
    public void PauseGame(bool enable)
    {
        _ui.PauseButton(enable);
    }
    public void ChangeSceneButton(string sceneName)
    {
        _ui.ChangeScene(sceneName);
    }
    private void OnEnable()
    {
        input.Enable();
    }
    private void OnDisable()
    {
        input.Disable();
    }
}